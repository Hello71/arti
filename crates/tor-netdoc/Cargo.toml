[package]
name = "tor-netdoc"
version = "0.2.0"
authors = ["The Tor Project, Inc.", "Nick Mathewson <nickm@torproject.org>"]
edition = "2021"
rust-version = "1.56"
license = "MIT OR Apache-2.0"
homepage = "https://gitlab.torproject.org/tpo/core/arti/-/wikis/home"
description = "Network document formats used with the Tor protocols."
keywords = ["tor", "arti"]
categories = ["parser-implementations"]
repository = "https://gitlab.torproject.org/tpo/core/arti.git/"

[features]
default = []

# Enable code to build the objects that represent different network documents.
build_docs = ["rand"]

# Enable the "router descriptor" document type, which is needed by relays and
# bridge clients.
routerdesc = []

# Enable the "ns consensus" document type, which some relays cache and serve.
ns_consensus = []

# Enable experimental APIs that are not yet officially supported.
#
# These APIs are not covered by semantic versioning.  Using this
# feature voids your "semver warrantee".
experimental-api = []

# Expose various struct fields as "pub", for testing.
#
# This feature is *super* dangerous for stability and correctness. If you use it
# for anything besides testing, you are probably putting your users in danger.
#
# The struct fields exposed by this feature are not covered by semantic version.
# In fact, using this feature will give you the opposite of a "semver
# guarantee": you should be mildly surprised when your code _doesn't_ break from
# version to version.
dangerous-expose-struct-fields = ["visible", "visibility"]

[dependencies]
tor-llcrypto = { path = "../tor-llcrypto", version = "0.2.0" }
tor-bytes = { path = "../tor-bytes", version = "0.2.0" }
tor-cert = { path = "../tor-cert", version = "0.2.0" }
tor-protover = { path = "../tor-protover", version = "0.2.0" }
tor-checkable = { path = "../tor-checkable", version = "0.2.0" }
tor-error = { path = "../tor-error", version = "0.2.0" }

base64 = "0.13.0"
bitflags = "1"
time = { version = "0.3", features = ["std", "parsing", "macros"] }
derive_more = "0.99"
digest = "0.10.0"
educe = "0.4.6"
hex = "0.4"
once_cell = "1"
phf = { version = "0.10.0", features = ["macros"] }
serde = "1.0.103"
signature = "1"
thiserror = "1"
visible = { version = "0.0.1", optional = true }
visibility = { version = "0.0.1", optional = true }
weak-table = "0.3.0"

rand = { version = "0.8", optional = true }

[dev-dependencies]
hex-literal = "0.3"
